FROM composer as build

WORKDIR /build

ADD . .
RUN composer install --no-dev --ignore-platform-reqs

FROM webdevops/php-nginx:alpine-php7

WORKDIR /app

COPY --from=build /build .

ENV WEB_DOCUMENT_ROOT /app/public

RUN chown -R 1000:1000 /app
RUN chmod -R 755 /app/storage
