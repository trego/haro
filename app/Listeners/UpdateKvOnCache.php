<?php

namespace App\Listeners;

use App\Events\KvUpdated;
use Illuminate\Support\Facades\Cache;

class UpdateKvOnCache
{
    /**
     * Handle the event.
     *
     * @param  KvUpdated $event
     * @return void
     */
    public function handle(KvUpdated $event)
    {
        $kv_pair = $event->kv_pair;

        Cache::put($kv_pair->key, $kv_pair);
    }
}
