<?php

namespace App\Events;

use App\KvPair;

class KvUpdated extends Event
{
    /**
     * @var KvPair
     */
    public $kv_pair;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(KvPair $kv_pair)
    {
        $this->kv_pair = $kv_pair;
    }
}
