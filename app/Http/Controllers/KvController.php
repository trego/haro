<?php

namespace App\Http\Controllers;

use App\KvPair;
use App\Events\KvUpdated;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class KvController extends Controller
{
    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $kv_pairs = KvPair::get();

            return response()->json($kv_pairs);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
        ]);

        try {
            $kv_pair = KvPair::find($request->input('key'));

            if (is_null($kv_pair)) {
                $kv_pair = KvPair::create([
                    'key' => $request->input('key'),
                    'value' => $request->input('value'),
                ]);
            } else {
                $kv_pair->update(['value' => $request->input('value')]);
                $kv_pair->refresh();
            }

            event(new KvUpdated($kv_pair));

            return response()->json($kv_pair);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * @param $key
     * @return Illuminate\Http\JsonResponse
     */
    public function show($key)
    {
        try {
            if (Cache::has($key)) {
                $kv_pair = Cache::get($key);
            } else {
                $kv_pair = KvPair::findOrFail($key);

                $this->rebuildCache();
            }

            return response()->json($kv_pair);
        } catch (ModelNotFoundException $e) {
            return response()->json('KV pair not found', 404);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Rebuilds the cache.
     *
     * @return void
     */
    private function rebuildCache()
    {
        Cache::flush();

        KvPair::get()->each(function ($kv_pair) {
            Cache::put($kv_pair->key, $kv_pair);
        });
    }

    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function handleRebuildCache()
    {
        try {
            $this->rebuildCache();

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
