<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KvPair extends Model
{
    /**
     * @var boolean
     */
    public $incrementing = false;

    /**
     * 
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];
}
