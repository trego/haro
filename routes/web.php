<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return response()->json([
        'name' => 'Trego/Haro',
        'version' => 1,
    ]);
});

$router->get('kv', 'KvController@index');
$router->post('kv', 'KvController@store');
$router->get('kv/{key}', 'KvController@show');
$router->post('rebuild-cache', 'KvController@handleRebuildCache');